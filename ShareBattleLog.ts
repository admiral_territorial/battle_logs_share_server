import * as mongo from 'mongodb'

// const DB_CONN_STRING="mongodb+srv://localhost"
const DB_CONN_STRING = process.env.DB_CONN_STRING || "mongodb://localhost:27017"
const THREE_DAYS = 259200; // in seconds

interface IRow {
  [key:string]: string
}

interface IBattleLogRecord {
  title:string;
  data:string;
  timestamp:number;
}

const valid_keys = [
  "Player Name|Player Level|Outcome|Ship Name|Ship Level|Ship Strength|Ship XP|Officer One|Officer Two|Officer Three|Hull Health|Hull Health Remaining|Shield Health|Shield Health Remaining|Location|Timestamp",
  "Reward Name|Count",
  "Fleet Type|Attack|Defense|Health|Ship Ability|Captain Maneuver|Officer One Ability|Officer Two Ability|Officer Three Ability|Officer Attack Bonus|Damage Per Round|Armour Pierce|Shield Pierce|Accuracy|Critical Chance|Critical Damage|Officer Defense Bonus|Armour|Shield Deflection|Dodge|Officer Health Bonus|Shield Health|Hull Health|Impulse Speed|Warp Range|Warp Speed|Cargo Capacity|Protected Cargo|Mining Bonus|Debuff applied|Buff applied",
  "Round|Battle Event|Type|Attacker Name|Attacker Alliance|Attacker Ship|Attacker - Is Armada?|Target Name|Target Alliance|Target Ship|Target - Is Armada?|Critical Hit?|Hull Damage|Shield Damage|Mitigated Damage|Total Damage|Ability Type|Ability Value|Ability Name|Ability Owner Name|Target Defeated|Target Destroyed|Charging Weapons %",
  // allow new logs with isolytic damage
  "Round|Battle Event|Type|Attacker Name|Attacker Alliance|Attacker Ship|Attacker - Is Armada?|Target Name|Target Alliance|Target Ship|Target - Is Armada?|Critical Hit?|Hull Damage|Shield Damage|Mitigated Damage|Mitigated Isolytic Damage|Total Damage|Total Isolytic Damage|Ability Type|Ability Value|Ability Name|Ability Owner Name|Target Defeated|Target Destroyed|Charging Weapons %"
]

class ShareBattleLog {
  mongo: any;
  constructor() {
    this.mongo = new mongo.MongoClient(DB_CONN_STRING);
    // this.redis = createClient( { socket: { host: "redis" } });
  }
  validate(data:Array<Array<IRow>>):boolean {
    for(let table of data) {
      let keys = Object.keys(table[0]).join("|")
      if(!valid_keys.includes(keys)) return false;
    }
    return true;
  }
  async store(id: string, data: string) {
    return new Promise(async (resolve, reject) => {

      try {
        await this.mongo.connect();
        const db = this.mongo.db('battle_logs');
        const logs = db.collection('logs');
        let key_exists = await logs.findOne({ title: id });
        if (key_exists) {
          id = `${id}_${Date.now()}`;
        }
        await logs.insertOne({
          title: id,
          data: data,
          timestamp: Date.now()
        });
        resolve(id);
      } catch (e) {
        reject({ 
          "error": "failed to store log",
          "message": e
        });
      }
      this.mongo.close();
    });
  }
  async fetch(id: string): Promise<string> {
    await this.mongo.connect();
    const db = this.mongo.db("battle_logs");
    const logs = db.collection("logs");
    let doc:IBattleLogRecord = await logs.findOne({ title: id });
    return new Promise((resolve, reject) => {
      this.mongo.close();
      if (!doc) {
        reject("Share link is invalid or expired");
      } else {
        resolve(JSON.parse(doc.data));
      }
    });
  }
}

export default ShareBattleLog
