import express from "express"
import ShareBattleLog from "./ShareBattleLog"

const app = express()
app.use(express.json({ limit: '1mb' }))
 
const share = new ShareBattleLog()

app.get("/", (req, res) => {
  res.sendFile(process.cwd() + "/public/index.html");
});

app.get("/api/heartbeat", (req, res) => {
  res.send({
    "status": "available"
  });
});

app.put("/api/:share_id", (req, res) => {
  let share_id = req.params.share_id;
  let data = req.body;
  if(share.validate(data)) {
    share.store(share_id, JSON.stringify(data)).then(data => {
      res.send({
        "status": "success",
        "id": data
      });
    }).catch(data => {
      res.send(data);
    });  
  } else {
    res.send({
      "alert": "Invalid battle log"
    })
  }
});

app.get("/api/:share_id", (req, res) => {
  let share_id = req.params.share_id;
  share.fetch(share_id).then(data => {
    res.json(data);
  }).catch(e => {
      res.status(404).json({
        "alert": e 
      });
    });
});

app.use(express.static('public'))

app.listen(9000)

